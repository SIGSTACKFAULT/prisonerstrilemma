class HistoricAverage:
    PAYOFFS = {
        "C":{"C":3,"N":1,"D":5},
        "N":{"C":4,"N":2,"D":2},
        "D":{"C":0,"N":3,"D":1}}
    def __init__(self):
        self.history = []
    def round(self, last):
        if(last != None):
            self.history.append(last)
        payoffsum = {"C":0, "N":0, "D":0}
        for move in self.history:
            for x in payoffsum:
               payoffsum[x] += HistoricAverage.PAYOFFS[move][x]
        return max(payoffsum, key=payoffsum.get)
