import random

class AllC:
    def round(self, _): return "C"
class AllN:
    def round(self, _): return "N"
class AllD:
    def round(self, _): return "D"
class RandomBot:
    def round(self, _): return random.choice(["C", "N", "D"])

class TitForTat:
	def round(self, last):
		if(last=="D"): return "D"
		return "C"
