from .stock         import AllC, AllN, AllD, RandomBot
from .grudger       import FastGrudger
from .titfortat     import TitForTat

from .tatfortit     import TatForTit
from .lastoptimal   import LastOptimalBot
from .nevercoop     import NeverCOOP
from .evaluater     import EvaluaterBot
from .jade          import Jade
from .nash          import Nash, Nash2
from .oldt4t        import OldTitForTat
from .historic      import HistoricAverage
from .patternfinder import PatternFinder
from .copycat       import CopyCat
from .ensemble      import Number6, Ensemble
from .weightedavg   import WeightedAverage
from .handshake     import HandshakeBot
from .dirichlet     import DirichletDice
from .tetragram     import Tetragram


botlist = [
	AllC, AllN, AllD, RandomBot, FastGrudger, TitForTat,
	TatForTit,
	LastOptimalBot,
	NeverCOOP,
	EvaluaterBot,
	Jade,
	Nash, Nash2,
	OldTitForTat,
	HistoricAverage,
	PatternFinder,
	CopyCat,
	Number6, Ensemble,
	WeightedAverage,
	HandshakeBot,
	DirichletDice,
	Tetragram
]
