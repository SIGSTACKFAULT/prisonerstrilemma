#!/usr/bin/env python3
import sys
import itertools
import threading

from bots import botlist

rounds = 0

def fix(v):
	if(str(v).upper() in ["C","N","D"]): return v
	else: return "N"

def payoff(a,b):
	if(a=="C"):
		if(b=="C"):
			return (3,3) #CC
		if(b=="N"):
			return (4,1) #CN
		if(b=="D"):
			return (0,5) #CD
	if(a=="N"):
		if(b=="C"):
			return (1,4) #NC
		if(b=="N"):
			return (2,2) #NN
		if(b=="D"):
			return (3,2) #ND
	if(a=="D"):
		if(b=="C"):
			return (5,0) #DC
		if(b=="N"):
			return (3,2) #DN
		if(b=="D"):
			return (1,1) #DD

botd = {b:0 for b in botlist}

matchups = itertools.product(botlist,botlist)
	 
class workThread (threading.Thread):
	def __init__(self, n, a, b):
		threading.Thread.__init__(self)
		self.a = a
		self.b = b
		self.n = n
	def run(self):
		process_data(self.n, self.a, self.b)
	 
def process_data(n, a, b):
	print(f"Game {n+1}/{ngames}: {a.__name__} vs. {b.__name__}")
	bota = a()
	botb = b()
			
	lasta = None
	lastb = None
	
	for i in range(rounds):
		choicea = fix(bota.round(lastb))
		choiceb = fix(botb.round(lasta))
	
		scorea, scoreb = payoff(choicea, choiceb)
		botd[a] += scorea
		botd[b] += scoreb
			
		lasta = choicea
		lastb = choiceb	
		 
if __name__ == '__main__':
	nbots = len(botlist)

	try:
		rounds = int(sys.argv[1])
		rounds = max(rounds,1)
	except:
		print(f"usage: {sys.argv[0]} ROUNDS")
		exit(1)

	ngames = nbots**2
	print("{} bots, {} games.".format(nbots, ngames))

	threads = []

	for n,(a,b) in enumerate(matchups):
		thread = workThread(n, a, b)
		thread.start()
		threads.append(thread)

	for t in threads:
		t.join()
	
	print()
	
	width = 10
	for bot in botlist:
		width = max(len(bot.__name__), width)

	print(f"{'name':{width}} | score/bot/round")
	print("-"*width,"-|----------------",sep="")
	
	for bot, score in sorted(botd.items(),key=lambda x:x[1], reverse=True):
		adjusted = score/rounds/nbots/2
		print(f"{bot.__name__:{width}} | {adjusted:0.3f}")
